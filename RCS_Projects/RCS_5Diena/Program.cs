﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace RCS_5Diena
{
    class Program
    {
        static void Main(string[] args)
        {
            Json();

        }

        public static void RakstLas()
        {
            //using (StreamWriter writer = new StreamWriter("teksts.txt")) // Pārrakstīt
            //{
            //    writer.WriteLine("Pirmais teksts failā, Update!!!");
            //}


            // IERAKSTĪŠANA //
            using (StreamWriter writer = File.AppendText("teksts.txt")) // Papildināt
            {
                writer.WriteLine("Pirmais teksts failā, Update!!!");
            }


            // NOLASĪŠANA //
            List<string> tekstaSaraksts = new List<string>(); // to, ko nolasām liekam sarakstā
            using (StreamReader reader = new StreamReader("teksts.txt")) // Nolasīt
            {
                string line; // Nolasam pirmo rindiņu
                while ((line = reader.ReadLine()) != null)// cikls, jo nezinam cik rindiņas būs failā.Lasām kamēr rindiņa nav tukša
                {
                    tekstaSaraksts.Add(line);
                }
            }
            foreach (string tekstaRinda in tekstaSaraksts)
            {
                Console.WriteLine(tekstaRinda);
            }
            Console.ReadKey();

        }

        public static void Json()
        {
            // JSON //
            QuizQuestion quizQuestion = new QuizQuestion();
            quizQuestion.Question = "Jautājums?";
            quizQuestion.Answer = "Atbilde?";

            string json = JsonConvert.SerializeObject(quizQuestion); // objektu pārveido par json formātu

            using (StreamWriter writer = new StreamWriter("MyJason.txt")) // Pārrakstīt
            {
                writer.WriteLine(json);
            }

            string jsonFromFile;
            using (StreamReader reader = new StreamReader("MyJason.txt")) // Nolasīt
            {
                jsonFromFile = reader.ReadToEnd();
            }
            QuizQuestion deserialized = JsonConvert.DeserializeObject<QuizQuestion>(jsonFromFile);

            Console.WriteLine(deserialized.Question);
            Console.WriteLine(deserialized.Answer);

            Console.ReadKey();

            // File.WriteAllText("teksts.txt",String.Empty);// notīrit tekstu
        }

    }
}
