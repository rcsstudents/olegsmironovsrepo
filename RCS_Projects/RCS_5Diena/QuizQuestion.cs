﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_5Diena
{
    [Serializable] // pieliekam atributu - elements, kas kaut kādā veidā apzīmē vai apraksta klasi
    class QuizQuestion
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
