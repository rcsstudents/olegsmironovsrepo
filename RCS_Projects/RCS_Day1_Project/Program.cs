﻿using System;
using System.Collections.Generic;

namespace RCS_Day1_Project
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exit = false;
            while (!exit)
            {
                Console.Clear();
                List<string> userAnswer = new List<string>();// jauns saraksts kas GLABĀS string elementus
                int score = 0;
                // string answer = string.Empty; kad ir BEZ saraksta vajag definēt string
                // vai string answer = "";

                /*
                for (int i = 0; i < DefinedQuestions.QuizQuestion.Count; i++)
                {
                    Console.WriteLine(DefinedQuestions.QuizQuestion[i].Question);
                    answer += Console.ReadLine() + "\n";
                }
                */
                /*
                int i = 0;
                while (i < DefinedQuestions.QuizQuestion.Count)
                {
                    Console.WriteLine(DefinedQuestions.QuizQuestion[i].Question);
                    answer += Console.ReadLine() + "\n";
                    i++;
                }
                */

                // Uzdodam jautajumus
                /*foreach (QuizQuestion question in DefinedQuestions.QuizQuestion)
                        // vai liek "var" QuizQuestion vietā
                {
                    Console.WriteLine(question.Question);
                    string answer = Console.ReadLine();
                    userAnswer.Add(answer);
                }
                // Izdrukājam atbildes
                for (int i =0; i < userAnswer.Count; i++)
                {
                    Console.WriteLine("Pareiza atbile: " + DefinedQuestions.QuizQuestion[i].Answer);
                    Console.WriteLine("Tu atbildēji: " + userAnswer[i]);
                }
                Console.ReadKey();
                }   */

                //DefinedQuestions.QuizQuestion.Shuffle()//--> JAUKT JAUTĀJUMUS SECĪBĀM
                DefinedQuestions.QuizQuestion.Shuffle();
                foreach (QuizQuestion question in DefinedQuestions.QuizQuestion)
                // vai liek "var" QuizQuestion vietā
                {
                    Console.WriteLine(question.Question);
                    string answer = Console.ReadLine();
                    if (answer == question.Answer)
                    {
                        score++;
                    }
                    userAnswer.Add(answer);
                }
                // Izdrukājam atbildes
                for (int i = 0; i < userAnswer.Count; i++)
                {
                    Console.WriteLine("Pareiza atbile uz jautājumu " + (i+1) + " : " + DefinedQuestions.QuizQuestion[i].Answer);
                    Console.WriteLine("Tu atbildēji: " + userAnswer[i]);
                }
                Console.WriteLine("Rezultāts ir: " + score);
                Console.WriteLine("Max punktu skaits: " + userAnswer.Count + "\n");

                Console.WriteLine("Vai pildīt testu vēlreiz? y or n");
                string choice = Console.ReadLine();
                if (choice.ToLower() == "n")
                {
                    exit = true;
                    Console.WriteLine("Paldies par piedalīšanos! ;)");
                    Console.ReadLine();
                }
            }
        }   
    }
}