﻿using System;
using System.Collections.Generic;

namespace DatuTipi
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MyDataTypes> myDataTypesList = new List<MyDataTypes>();

            Console.WriteLine("Izpildīt programmu? (y/n)");
            while (Console.ReadLine() != "n") 
            {
                MyDataTypes myDataTypes = new MyDataTypes();// Uztaisam jaunu objektu, jo klase kļuva par publisku, nevis statisku
                // 1.BOOL
                Console.WriteLine("Ievadi bool vērtību (true/false)");
                string myBoolString = Console.ReadLine();
                myDataTypes.MyBool = Convert.ToBoolean(myBoolString);
                
                // 2.BYTE
                Console.WriteLine("Ievadi byte vērtību (true/false)");
                string myByteString = Console.ReadLine();
                // bool isConverted = byte.TryParse(myByteString, out byte result);
                // if (isConverted == true)
                // {
                    myDataTypes.MyByte = Convert.ToByte(myByteString);
                // }

                // 3.SHORT
                Console.WriteLine("Ievadi short vērtību (true/false)");
                string myShortString = Console.ReadLine();
                myDataTypes.MyShort = Convert.ToInt16(myShortString);

                // 4.INT
                Console.WriteLine("Ievadi Int vērtību (true/false)");
                string myIntString = Console.ReadLine();
                myDataTypes.MyInt = Convert.ToInt32(myIntString);

                // 5.LONG
                Console.WriteLine("Ievadi Long vērtību (true/false)");
                string myLongString = Console.ReadLine();
                myDataTypes.MyLong = Convert.ToInt64(myLongString);

                // 6.DOUBLE
                Console.WriteLine("Ievadi Double vērtību (true/false)");
                string myDoubleString = Console.ReadLine();
                myDataTypes.MyDouble = Convert.ToDouble(myDoubleString);

                // 7.DECIMAL
                Console.WriteLine("Ievadi Decimal vērtību (true/false)");
                string myDecimalString = Console.ReadLine();
                myDataTypes.MyDecimal = Convert.ToDecimal(myDecimalString);

                // 8.STRING
                Console.WriteLine("Ievadi String vērtību (true/false)");
                myDataTypes.MyString = Console.ReadLine();

                // 9.CHAR
                Console.WriteLine("Ievadi Char vērtību (true/false)");
                string myCharString = Console.ReadLine();
                myDataTypes.MyChar = Convert.ToChar(myCharString);

                myDataTypesList.Add(myDataTypes);
            }

            foreach (MyDataTypes myData in myDataTypesList)
            {
                Console.WriteLine(myData.MyBool);
                Console.WriteLine(myData.MyByte);
                Console.WriteLine(myData.MyShort);
                Console.WriteLine(myData.MyInt);
                Console.WriteLine(myData.MyLong);
                Console.WriteLine(myData.MyDouble);
                Console.WriteLine(myData.MyDecimal);
                Console.WriteLine(myData.MyString);
                Console.WriteLine(myData.MyChar);
            }

            Console.ReadKey();

        }
    }
}
