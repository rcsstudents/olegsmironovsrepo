﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatuTipi
{
    /*public static class MyDataTypes // Nav jātaisa jauns objekts, klasi var izsaukt uzreiz, jo STATIC
    {
        public static bool MyBool { get; set; }
        public static byte MyByte { get; set; }
        public static short MyShort { get; set; }
        public static int MyInt { get; set; }
        public static long MyLong { get; set; }
        public static double MyDouble { get; set; }
        public static decimal MyDecimal { get; set; }
        public static string MyString { get; set; }
        public static char MyChar { get; set; }
    }*/
    public class MyDataTypes // Nav jātaisa jauns objekts, klasi var izsaukt uzreiz, jo STATIC
    {
        public bool MyBool { get; set; }// MyBool ir klases parametrs
        public byte MyByte { get; set; }
        public short MyShort { get; set; }
        public int MyInt { get; set; }
        public long MyLong { get; set; }
        public double MyDouble { get; set; }
        public decimal MyDecimal { get; set; }
        public string MyString { get; set; }
        public char MyChar { get; set; }
        public void Method()// Method() ir metode
        {
        bool True;
        }
    }
}
