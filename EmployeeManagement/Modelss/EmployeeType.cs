﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    [Serializable]
    public enum EmployeeType
    {
        Developer = 10,
        Manager = 20
    }
}
