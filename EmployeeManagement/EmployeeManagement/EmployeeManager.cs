﻿using DataManager;
using DataManager.ManageData;
using Models.EmployeeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmployeeManagement
{
    public class EmployeeManager
    {

        public void ManageEmployees()
        {
            DataReader reader = new DataReader(DataConfiguration.EmployeeData);
            List<Employee> employeeList = reader.ReadData<Employee>();

            foreach (Employee employee in employeeList)
            {
                Console.WriteLine($"Full name: {employee.FullName()}");
                Console.WriteLine($"Age: {employee.Age}");
                Console.WriteLine($"Vacation: {employee.Vacation.Reason}");
                Console.WriteLine($"VacationStart: {employee.Vacation.StartDate}");
                Console.WriteLine($"VacationEnd: {employee.Vacation.EndDate}");
                Console.WriteLine();
            }
        }
    }
}
