﻿using Models;
using System;

namespace EmployeeManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Employee Management System!");
            Console.WriteLine("Employee");
            try
            {
                EmployeeManager employeeManager = new EmployeeManager();
                employeeManager.ManageEmployees();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadKey();
        }
    }
}
