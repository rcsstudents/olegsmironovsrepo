﻿using DataManager;
using DataManager.ManageData;
using Models.EmployeeModels;
using System;
using System.Collections.Generic;

namespace EmployeeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Employee Generator!");
            List<Employee> employeeList = new List<Employee>();

            bool exit = false;
            while (!exit)
            {
                try// nepieciešams, lai ķertu kļūdas
                {
                    
                    GenerateEmployees generateEmployee = new GenerateEmployees();
                    Employee employee = generateEmployee.GenerateEmployee();
                    employeeList.Add(employee);

                }
                catch (Exception ex)// ķer kļūdas
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadKey();
                }
                Console.WriteLine("Vai pievienot jaunu darbinieku? (y/n)");
                string choice = Console.ReadLine();
                if (choice == "n")
                {
                    exit = true;
                }
            }
            DataWriter writer = new DataWriter(DataConfiguration.EmployeeData);
            writer.WriteData(employeeList);
        }
    }
}
