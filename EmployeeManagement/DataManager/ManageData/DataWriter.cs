﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace DataManager.ManageData
{
    public class DataWriter
    {
        public readonly string FileName;// parametru nevarēs izmainīt, vērtību var uzstādīt tikai caur konstruktoru.

        public DataWriter(string fileName)
        {
            FileName = fileName;
        }
        
        public void WriteData<T>(List<T> data)
        {
            using (StreamWriter writer = new StreamWriter(FileName))
            {
                string dataJson = JsonConvert.SerializeObject(data);
                writer.WriteLine(dataJson);
            }
        }
    }
}
