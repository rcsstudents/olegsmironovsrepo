﻿using DataManager;
using DataManager.ManageData;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityManagement.UniversityManagement
{
    public class StudentManager : IManager
    {
        List<Student> Students;
        DataReader reader;
        DataWriter writer;

        public StudentManager()
        {
            reader = new DataReader(DataConfiguration.StudentData);
            writer = new DataWriter(DataConfiguration.StudentData);
            Students = reader.ReadData<Student>();
        }

        public void Create()
        {
            Console.WriteLine("Pievienot jaunu studentu: ");
            Console.Write("Studenta id: ");
            Student student = new Student();
            string idString = Console.ReadLine();
            int id = Convert.ToInt32(idString);
            student.Id = id;
            Console.Write("Studenta vārds: ");
            student.Name = Console.ReadLine();

            Students.Add(student);
            writer.WriteData(Students);
        }

        public void Delete()
        {
            Console.WriteLine("Dzēst esošo studentu: ");
            Console.Write("Studenta id: ");
            string idString = Console.ReadLine();
            int id = Convert.ToInt32(idString);
            Students = Students.Where(u => u.Id != id).ToList();
            writer.WriteData(Students);
        }

        //public void Read() // UZLABOT METODI
        //{

        //    foreach(Student student in Students)
        //    {
        //        Console.WriteLine($"{student.Id} {student.Name}");
        //    }
        //}

        public void Read()
        {
            UniversityManager universityManager = new UniversityManager();
            foreach (Student student in Students)
            {
                Console.WriteLine($"{student.Id} {student.Name} studies in");
                University studentsUniversity = universityManager.Universities.FirstOrDefault(u => u.Id == student.UniversityId);
                Console.WriteLine($"{ studentsUniversity.Id} {studentsUniversity.Name}");
            }
        }

        public void Update()
        {
            Console.WriteLine("Mainīt esošā studenta datus: ");
            Console.Write("Studenta id: ");
            string idString = Console.ReadLine();
            int id = Convert.ToInt32(idString);
            Student student = Students.FirstOrDefault(u => u.Id == id);
            if(student == null)
            {
                Console.WriteLine($"Students ar Id={id} neeksistē");
                return;
            }
            Console.Write($"Mainīt studenta ar id{id} vārdu uz: ");
            student.Name = Console.ReadLine();

            writer.WriteData(Students);
        }
    }
}
