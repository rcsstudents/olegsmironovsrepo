﻿using DataManager;
using DataManager.ManageData;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityManagement.UniversityManagement
{
    public class UniversityManager : IManager
    {
        public List<University> Universities;
        DataReader reader;
        DataWriter writer;

        public UniversityManager()
        {
            reader = new DataReader(DataConfiguration.UniversityData);
            writer = new DataWriter(DataConfiguration.UniversityData);
            Universities = reader.ReadData<University>();
        }

        public void Create()
        {
            Console.WriteLine("Pievienot jaunu universitāti: ");
            Console.Write("Universitātes id: ");
            University university = new University();
            string idString = Console.ReadLine();
            int id = Convert.ToInt32(idString);
            university.Id = id;
            Console.Write("Universitātes nosaukums: ");
            university.Name = Console.ReadLine();

            Universities.Add(university);
            writer.WriteData(Universities);
        }

        public void Delete()
        {
            Console.WriteLine("Dzēst esošo universitāti: ");
            Console.Write("Universitātes id: ");
            string idString = Console.ReadLine();
            int id = Convert.ToInt32(idString);
            Universities = Universities.Where(u => u.Id != id).ToList();
            writer.WriteData(Universities);
        }

        public void Read()
        {
            foreach (University university in Universities)
            {
                Console.WriteLine($"{university.Id} {university.Name}");
            }
        }

        public void Update()
        {
            Console.WriteLine("Mainīt esošās universitātes datus: ");
            Console.Write("Universitātes id: ");
            string idString = Console.ReadLine();
            int id = Convert.ToInt32(idString);
            University university = Universities.FirstOrDefault(u => u.Id == id);
            if (university == null)
            {
                Console.WriteLine($"Universitāte ar Id={id} neeksistē");
                return;
            }
            Console.Write($"Mainīt universitātes ar id{id} nosaukumu uz: ");
            university.Name = Console.ReadLine();

            writer.WriteData(Universities);
        }
    }
}
