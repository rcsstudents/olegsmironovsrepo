﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniversityManagement
{
    interface IManager
    {
        void Create();
        void Read();
        void Update();
        void Delete();
    }
}
